"use strict";
document.addEventListener("DOMContentLoaded", setup);

function setup()
{
    let btns = document.querySelectorAll('.button-flag');
    
    for (let btn of btns)
    {
        btn.addEventListener("click", flag);
    }
}

function flag(e)
{
    let text = e.target.textContent
    let unflag = "UNFLAG";

    if(text.localeCompare(unflag) == 0)
    {
        e.target.textContent = "FLAG";
    }

    else
    {
        e.target.textContent = "UNFLAG";
    }
}