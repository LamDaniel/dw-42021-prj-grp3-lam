"use strict";
//Grigor Mihaylov 1937997

document.addEventListener("DOMContentLoaded", setup);

function setup() {
    //let form = document.querySelector("#chats"); //modify
    //  form.addEventListener("submit", submit);
    let buttons = document.querySelectorAll('.chat');

    for (let b of buttons) {
        b.addEventListener("click", pick);
    }
}
function pick(evt){
        document.querySelector("#newChatButton").disabled = false;

        let uname = evt.currentTarget.querySelector('p').textContent;
        const span = document.querySelector('#selected');
        span.textContent=uname
        const targetUsr = document.querySelector("#id_targetUsr");
        targetUsr.value=uname;
}

/*
let id = document.querySelector("#id_chat")
id.textContent="hello"

msg = document.querySelectorAll(".emptyListMessage")
msg[1].style.visibility = "hidden"


let btn = document.querySelector("#sentMsg")
btn.disabled = true;
*/