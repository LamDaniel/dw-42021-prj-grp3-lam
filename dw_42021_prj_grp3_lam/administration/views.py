from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth.models import User, Group
from .forms import UserForm

#Testing Method
def test():
    all_users = User.objects.all()
    print(all_users)

    #all_groups = Group.objects.all()
    #print(all_groups)

    #query = all_users.union(all_groups).order_by('username')
    #print(query)

    query_string = 'SELECT us.id, us.username, gr.name FROM auth_user us \
                    JOIN auth_user_groups ug ON us.id = ug.user_id \
                    JOIN auth_group gr ON ug.group_id = gr.id'

 
    query = User.objects.raw(query_string)

    for n in query:
        print(n.id, n.username, n.name)

    #users_names = all_users.username
    #print(users_names)
    #user_groups = 
#test()

# Create your views here.
def index(request):
    query_string = 'SELECT us.id, us.username, gr.name FROM auth_user us \
                    JOIN auth_user_groups ug ON us.id = ug.user_id \
                    JOIN auth_group gr ON ug.group_id = gr.id'

    query_set = User.objects.raw(query_string)
    context = {'users' : query_set, 'tabTitle' : 'Administration Page', 'author' : 'Vladyslav Berezhnyak (1934443)'}
    return render(request, 'administration/index.html', context)

def deleteUser(request, pk):
    query_set = User.objects.get(id=pk)
    
    if request.method == "POST":
        query_set.delete()
        return redirect('/administration/')
    
    context = {'user': query_set}
    return render(request, 'administration/delete.html', context)