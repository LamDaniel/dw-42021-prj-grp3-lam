from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('delete_user/<str:pk>', views.deleteUser, name="delete_user")
]