from django.contrib.auth import authenticate
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import RegForm, LogForm
from .models import User


# Create your views here.
def index(request):
    tabTitle = 'Index'
    return render(request, 'usermanagement/index.html', {'tabTitle': tabTitle})


def register(request):
    tabTitle = 'Register'
    if request.method == 'POST':
        form = RegForm(request.POST, request.FILES)

        if form.is_valid():
            data = form.cleaned_data

            if form.check_correct(data["username"], data["password"], data["verifypass"]):
                if data["avatar"] is None:
                    u = User(username=data["username"], email=data["email"])
                else:
                    u = User(avatar=request.FILES['avatar'], username=data["username"], email=data["email"])
                u.set_password(data["password"])

                u.save()
                return HttpResponseRedirect('/usermanagement/login')

    else:
        form = RegForm()

    return render(request, 'usermanagement/register.html', {'form': form, 'tabTitle': tabTitle})


def log_in(request):
    tabTitle = "Log In"

    if request.method == 'POST':
        form = LogForm(request.POST)

        if form.is_valid():
            data = form.cleaned_data

            if not authenticate(username=data['username'], password=data['password']) is None:
                return HttpResponseRedirect('/documentaries')

            if form.check_data(data["username"]):
                request.session['curr_user'] = form.clean_dict(
                    model_to_dict(User.objects.get(username=data['username'])))
                print(request.session['curr_user'])
                return HttpResponseRedirect('/documentaries')

    else:
        form = LogForm()

    return render(request, 'usermanagement/login.html', {'form': form, 'tabTitle': tabTitle})

#Created by Daniel Lam (1932789)
def logout(request):
    request.session['curr_user'] = None
    return render(request, 'usermanagement/logout.html', {})