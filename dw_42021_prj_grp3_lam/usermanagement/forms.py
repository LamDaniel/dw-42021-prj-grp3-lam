from django import forms
from django.contrib.auth.hashers import check_password

from .models import User


class RegForm(forms.Form):
    avatar = forms.ImageField(required=False)
    username = forms.CharField(label='Username', max_length=32)
    password = forms.CharField(label='Password', max_length=64, widget=forms.PasswordInput)
    verifypass = forms.CharField(label='Repeat Password', max_length=64, widget=forms.PasswordInput)
    email = forms.EmailField(label='Email', max_length=128)

    class Meta:
        model = User
        fields = ['avatar', 'username', 'password', 'email']

    def check_correct(self, aUname, aPass, aVPass):
        if User.objects.filter(username=aUname).exists():
            self.add_error("username", "A user with this username already exists.")
            return False

        if not aPass == aVPass:
            self.add_error('password', "Your passwords don't match, please try again.")
            return False

        return True


class LogForm(forms.Form):
    username = forms.CharField(label='Username', max_length=32)
    password = forms.CharField(label='Password', max_length=256, widget=forms.PasswordInput)

    def check_data(self, attempt):
        if not User.objects.filter(username=attempt).exists():
            self.add_error(None, "Username or password incorrect, try again or register.")
            return False

        else:
            p = User.objects.get(username=attempt).password

            if check_password(self.cleaned_data["password"], p):
                return True

            else:
                self.add_error(None, "Username or password incorrect, try again or register.")
                return False

    def clean_dict(self, attempt):
        attempt['avatar'] = str(attempt['avatar'])

        del attempt['register_date']
        del attempt['password']
        del attempt['last_login']
        return attempt
