from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser


# Create your models here.
class User(AbstractBaseUser):
    avatar = models.ImageField(upload_to='avatars/', default='noavatar.jpeg', max_length=100)
    username = models.CharField(max_length=32, unique=True)
    email = models.EmailField(max_length=128)
    register_date = models.DateField(default=timezone.now)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        return "{uname}, Email: {email}, Registered on: {regDate}".format(uname=self.username,
                                                                          email=self.email,
                                                                          regDate=self.register_date)
