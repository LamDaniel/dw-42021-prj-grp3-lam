#Grigor Mihaylov 1937997
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),

    path('newChat', views.newChat, name='newChat')
]
