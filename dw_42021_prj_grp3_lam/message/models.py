#Grigor Mihaylov 1937997
from django.db import models
from usermanagement.models import User

from django.utils import timezone

# Create your models here.

class Chats(models.Model):
    id = models.AutoField(primary_key=True)
    members = models.ManyToManyField(User)

    def __str__(self):
        objInStr="Chat with: "
        for m in self.members:
            objInStr = objInStr + m.username +", "

        return objInStr

class Messages(models.Model):

    messageContent = models.CharField(max_length=1000)
    chat = models.ForeignKey(Chats,  on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timeStamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        objInStr= "Message from: " +self.user.username +" in "+self.chat;
        return objInStr