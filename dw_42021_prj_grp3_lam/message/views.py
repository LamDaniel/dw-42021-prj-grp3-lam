#Grigor Mihaylov 1937997
from django.shortcuts import render
from .forms import ChatForm, MessagesCreate
# Create your views here.
def index(request):
    request.session['user_id']= 'grigor' #TEMOPRARRY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    context = {}
    context['author'] = 'Grigor Mihaylov (1937997)'
    auth = request.session.get('user_id', '')
    if (not auth):
        context['tabTitle'] = 'Not logged in'
        return render(request, 'message/noLogin.html', context)



    if request.method == 'POST':
        context['messageMakeForm'] = MessagesCreate()
        context['tabTitle'] = 'Chat'
        context['uname'] = 'UserName'  # TEMOPRARRY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        data1 = range(0);  # TEMOPRARRY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        context['testList'] = data1

        form =MessagesCreate(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            return render(request, 'message/chatDisplay.html', context)
        else:
            form = ChatForm(request.POST)
            if form.is_valid():
                data = form.cleaned_data

                return render(request, 'message/chatDisplay.html', context)

    context['chatGetForm'] = ChatForm()
    data = range(10)  # TEMOPRARRY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    context['testList'] = data
    context['tabTitle'] = 'Messages'
    return render(request, 'message/index.html', context)






def newChat(request):
    request.session['user_id'] = 'grigor'  # TEMOPRARRY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    context = {}
    context['author'] = 'Grigor Mihaylov (1937997)'
    auth = request.session.get('user_id', '')
    if (not auth):
        context['tabTitle'] = 'Not logged in'
        return render(request, 'message/noLogin.html', context)

    form = ChatForm(request.POST)
    if form.is_valid():
        data = form.cleaned_data

        context['chatGetForm'] = ChatForm()
        context['tabTitle'] = 'Messages'
        return render(request, 'message/index.html', context)

    data = range(20); # TEMOPRARRY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    context['tabTitle'] = 'New Chat'
    context['testList'] = data
    context['chatMakeForm'] = ChatForm()

    return render(request, 'message/newChat.html', context)
