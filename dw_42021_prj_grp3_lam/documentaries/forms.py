from django import forms
from django.core.validators import MaxValueValidator


class DocumentaryForm(forms.Form):
    EX = "Expository"
    ES = "Essayistic"
    OB = 'Observational'
    PA = "Participatory"
    RE = "Research"
    IN = "Interview"
    NONE = None
    genreList = (
        (EX, 'Expository'),
        (ES, 'Essayistic'),
        (OB, 'Observational'),
        (PA, 'Participatory'),
        (RE, 'Research'),
        (IN, 'Interview'),
        (NONE, None),
    )

    PA = 'Parent'
    CE = 'Certificate'
    statusList = (
        (PA, "Parent's Guide"),
        (CE, 'Certificate'),
        (NONE, None),
    )
    filmMaker = forms.CharField(label="Director", max_length=100)
    title = forms.CharField(label="Title", max_length=30)
    genre = forms.ChoiceField(choices=genreList, label="Genre")
    keywords = forms.CharField(label="Keywords", max_length=300, required=False)
    description = forms.CharField(label="Description", widget=forms.Textarea)
    url = forms.URLField(label="URL", max_length=500)
    year = forms.IntegerField(label="Year Of Release")

    status = forms.ChoiceField(choices=statusList, label="Status")
    rate = forms.IntegerField(label="Rating (out of 100)", validators=[MaxValueValidator(100)])
    snapshot = forms.ImageField(label="Movie Preview", required=False)


class CommentForm(forms.Form):
    commentText = forms.CharField(label="", max_length=1000, widget=forms.Textarea(attrs={'class': 'commentText', 'placeholder': 'Write your comment here!'}))