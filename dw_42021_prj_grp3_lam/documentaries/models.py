from django.core.validators import MaxValueValidator
from django.db import models
from django.urls import reverse
from usermanagement.models import User
from django.utils.timezone import now

# Create your models here.
class Movie(models.Model):
    EX = "Expository"
    ES = "Essayistic"
    OB = 'Observational'
    PA = "Participatory"
    RE = "Research"
    IN = "Interview"
    NONE = None
    genreList = (
        (EX, 'Expository'),
        (ES, 'Essayistic'),
        (OB, 'Observational'),
        (PA, 'Participatory'),
        (RE, 'Research'),
        (IN, 'Interview'),
        (NONE, None),
    )

    PA = 'Parent'
    CE = 'Certificate'
    statusList = (
        (PA, "Parent's Guide"),
        (CE, 'Certificate'),
        (NONE, None),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    filmMaker = models.CharField(max_length=50)
    title = models.CharField(max_length=30)
    genre = models.CharField(max_length=30, choices=genreList, default=NONE)
    keywords = models.CharField(max_length=300)
    description = models.TextField()
    url = models.URLField(max_length=500)
    year = models.IntegerField()
    status = models.CharField(max_length=30, choices=statusList, default=NONE)
    rate = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(100)])
    snapshot = models.ImageField(upload_to='snapshots')

    def __str__(self):
        return "Title: " + self.title + ", Film Maker: " + self.filmMaker + ", Genre: " + self.genre + ", Description: " + str(
            self.description)

    def get_absolute_url(self):
        return reverse('allDocumentaries')


class Comment(models.Model):
    commentText = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=now)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

    def __str__(self):
        return "Comment: " + str(self.commentText) + ", User: " + self.user + ", Date: " + self.date + ", Movie: " + self.movie

