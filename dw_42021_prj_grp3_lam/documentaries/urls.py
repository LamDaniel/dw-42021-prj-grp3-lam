from django.urls import path

from . import views
from .views import DocumentariesList, DocumentaryDetail, DeleteDocumentary, EditDocumentary, Forbidden

urlpatterns = [
    path('', views.index, name='index'),
    path('allDocumentaries/', DocumentariesList.as_view(), name='allDocumentaries'),
    path('documentaryDetail/<int:pk>', DocumentaryDetail.as_view(), name='documentaryDetail'),
    path('addDocumentary/', views.addDocumentary, name='addDocumentary'),
    path('allDocumentaries/search/', views.searchDocumentaries, name="documentariesFilterList"),
    path('allDocumentaries/delete/<int:pk>', DeleteDocumentary.as_view(), name="deleteDocumentary"),
    path('allDocumentaries/edit/<int:pk>', EditDocumentary.as_view(), name="editDocumentary"),
    path('forbidden/', Forbidden.as_view(), name="forbidden")
]
