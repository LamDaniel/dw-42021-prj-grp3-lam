from django.contrib import messages
from django.http import HttpResponseRedirect, request
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, TemplateView, DetailView, DeleteView, UpdateView, FormView
from usermanagement.models import User
from django.utils.timezone import now

from .forms import DocumentaryForm, CommentForm
from .models import Movie, Comment

"""
class Index(TemplateView):

    template_name = "documentaries/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['tabTitle'] = "RateMyDocumentaries"
        return context
"""

def index(request):
    return render(request, 'documentaries/index.html', {'tabTitle': "RateMyDocumentaries", 'curr_user': request.session.get('curr_user', None)})


class DocumentariesList(ListView):
    model = Movie
    template_name = "documentaries/documentaries.html"
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tabTitle'] = "Documentaries Page"
        context['curr_user'] = self.request.session['curr_user']
        return context

    def get(self, request, *args, **kwargs):
        curr_user = request.session.get('curr_user', None)
        request.session['curr_user'] = curr_user
        return super().get(request, *args, **kwargs)


def searchDocumentaries(request):
    tabTitle = 'Search Result'
    if request.method == "POST":
        search = request.POST['search']
        documentariesList = Movie.objects.filter(title__icontains=search)
        return render(request, 'documentaries/search.html', {'search': search, 'documentariesList': documentariesList, 'tabTitle': tabTitle, 'curr_user': request.session['curr_user']})
    else:
        return render(request, 'documentaries/documentaries.html', {})


class DocumentaryDetail(DetailView, FormView):
    model = Movie
    template_name = "documentaries/documentaryDetail.html"
    form_class = CommentForm

    def form_valid(self, form):
        if self.request.session['curr_user'] is None:
            return HttpResponseRedirect('/usermanagement/login')
        movieId = self.kwargs['pk']
        data = form.cleaned_data
        Comment.objects.create(commentText=data['commentText'], user=User.objects.get(id=self.request.session['curr_user'].get('id')), date=now(), movie=Movie.objects.get(id=movieId))
        return HttpResponseRedirect('/documentaries/documentaryDetail/' + str(movieId))

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['tabTitle'] = "Documentary Detail"
        context['curr_user'] = self.request.session['curr_user']
        return context

    def get(self, request, *args, **kwargs):
        curr_user = request.session.get('curr_user', None)
        request.session['curr_user'] = curr_user
        return super().get(request, *args, **kwargs)


def addDocumentary(request):

    if request.session['curr_user'] is None:
        return HttpResponseRedirect('/usermanagement/login')

    tabTitle = 'Add Documentary'
    if request.method == 'POST':
        form = DocumentaryForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data

            if data["snapshot"] is None:
                #TO-DO: change user to authenticated user
                m = Movie(user=User.objects.get(id=request.session['curr_user'].get('id')), filmMaker=data['filmMaker'], title=data['title'],
                          genre=data['genre'],
                          keywords=data['keywords'], description=data['description'], url=data['url'],
                          year=data['year'], status=data['status'], rate=data['rate'], snapshot='noavatar.jpeg')

            else:
                m = Movie(user=User.objects.get(id=request.session['curr_user'].get('id')), filmMaker=data['filmMaker'], title=data['title'],
                          genre=data['genre'],
                          keywords=data['keywords'], description=data['description'], url=data['url'],
                          year=data['year'], status=data['status'], rate=data['rate'],
                          snapshot=request.FILES['snapshot'])

            m.save()

            return HttpResponseRedirect('/documentaries/allDocumentaries/')
    else:
        form = DocumentaryForm()

    return render(request, 'documentaries/addDocumentary.html', {'form': form, 'tabTitle': tabTitle, 'curr_user': request.session.get('curr_user', None)})


def commentSection(request):

    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/documentaries/documentaryDetail/<int:pk>')
    else:
        form = CommentForm()

    return render(request, 'documentaries/documentaryDetail.html')


class DeleteDocumentary(DeleteView):
    model = Movie
    template_name = "documentaries/deleteDocumentary.html"
    success_url = reverse_lazy('allDocumentaries')

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['tabTitle'] = "Documentary Detail"
        context['curr_user'] = self.request.session['curr_user']
        return context

    def get(self, request, *args, **kwargs):
        curr_user = request.session.get('curr_user', None)
        request.session['curr_user'] = curr_user
        if request.session['curr_user'] is None:
            return HttpResponseRedirect('/usermanagement/login')
        if request.session['curr_user'].get("id") is not User.objects.get(movie=Movie.objects.get(id=self.kwargs['pk'])).id:
            return HttpResponseRedirect('/documentaries/forbidden')
        return super().get(request, *args, **kwargs)

class EditDocumentary(UpdateView):
    model = Movie
    template_name = "documentaries/editDocumentary.html"
    fields = ['filmMaker', 'title', 'genre', 'keywords', 'description', 'url', 'year', 'status', 'rate', 'snapshot']

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['tabTitle'] = "Documentary Detail"
        context['curr_user'] = self.request.session['curr_user']
        return context

    def get(self, request, *args, **kwargs):
        curr_user = request.session.get('curr_user', None)
        request.session['curr_user'] = curr_user
        if request.session['curr_user'] is None:
            return HttpResponseRedirect('/usermanagement/login')
        if request.session['curr_user'].get("id") is not User.objects.get(movie=Movie.objects.get(id=self.kwargs['pk'])).id:
            return HttpResponseRedirect('/documentaries/forbidden')
        return super().get(request, *args, **kwargs)

class Forbidden(TemplateView):
    template_name = "documentaries/forbidden.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['tabTitle'] = "Documentary Detail"
        context['curr_user'] = self.request.session['curr_user']
        return context

    def get(self, request, *args, **kwargs):
        curr_user = request.session.get('curr_user', None)
        request.session['curr_user'] = curr_user
        return super().get(request, *args, **kwargs)